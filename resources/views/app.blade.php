<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {{-- Roboto font --}}
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&lang=en">

    {{-- Material icons --}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    {{-- Google MDL components --}}
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.light_green-deep_purple.min.css">
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>

    {{-- Vuejs --}}
    <script src="https://unpkg.com/vue"></script>

    {{-- MDL components for Vue --}}
    <script src="//rawgit.com/posva/vue-mdc/mdl/dist/vue-mdl.min.js"></script>

    {{-- Axios to make AJAX requests --}}
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>


    {{-- Reset some of the MDL styles --}}
    <style>
        *, *::before, *::after {
            box-sizing : inherit;
        }

        html {
            box-sizing : border-box;
        }

        [v-cloak] {
            visibility : hidden;
        }

        .mdl-layout {
            height     : auto;
            min-height : 100vh;
        }

        .mdl-card {
            overflow : visible;
        }

        .mdl-textfield__input[type="date"] {
            height : 18px;
        }

        .mdl-textfield[invalid="true"] .mdl-textfield__input {
            border-color : #d50000;
            box-shadow   : none;
        }

        .mdl-textfield[invalid="true"] .mdl-textfield__label:after {
            background-color : #d50000;
        }

        .mdl-textfield--floating-label[invalid="true"] .mdl-textfield__label {
            color     : #d50000;
            font-size : 12px;
        }

        .mdl-dialog-container .mdl-dialog {
            max-width  : 900px;
            max-height : 100%;
            overflow-y : auto;
        }

        .mdl-grid {
            max-width : 100%;
        }

        .mdl-grid--full-width {
            width      : 100%;
            overflow-x : auto;
        }

        .mdl-mini-footer {
            margin-top : auto;
        }
    </style>

</head>
<body>

<div class="mdl-layout mdl-js-layout" id="app" v-cloak>
    <main class="mdl-grid mdl-grid--full-width mdl-grid--no-spacing">
        <div class="mdl-cell mdl-cell--12-col mdl-card">

            <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Jobs</h2>
            </div>

            <div class="mdl-grid mdl-grid--full-width">
                <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-grid--no-spacing">
                    <mdl-textfield class="mdl-cell mdl-cell--3-col"
                                   :invalid="search && !jobs.length"
                                   floating-label="Search"
                                   v-model="search"></mdl-textfield>
                </div>

                <!--<editor-fold desc="Jobs Table">-->
                <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-grid--no-spacing">
                    <table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col mdl-shadow--4dp">
                        <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">First Name</th>
                            <th class="mdl-data-table__cell--non-numeric">Surname</th>
                            <th class="mdl-data-table__cell--non-numeric">Address</th>
                            <th class="mdl-data-table__cell--non-numeric">Suburb</th>
                            <th class="mdl-data-table__cell--non-numeric">Postcode</th>
                            <th class="mdl-data-table__cell--non-numeric">Date</th>
                            <th>Hours Worked</th>
                            <th class="mdl-data-table__cell--non-numeric">Details</th>
                            <th class="mdl-data-table__cell--non-numeric">Completed</th>
                            <th>Price</th>
                            <th class="mdl-data-table__cell--non-numeric">Paid</th>
                            <th class="mdl-data-table__cell--non-numeric"></th>
                        </tr>
                        </thead>
                        <tbody :class="{ 'mdl-color--grey-300': jobsLoading }">
                        <tr v-for="(job, key) in jobs">
                            <td class="mdl-data-table__cell--non-numeric">@{{ job.name }}</td>
                            <td class="mdl-data-table__cell--non-numeric">@{{ job.surname }}</td>
                            <td class="mdl-data-table__cell--non-numeric">@{{ job.address }}</td>
                            <td class="mdl-data-table__cell--non-numeric">@{{ job.suburb }}</td>
                            <td class="mdl-data-table__cell--non-numeric">@{{ job.postcode }}</td>
                            <td class="mdl-data-table__cell--non-numeric">@{{ job.date }}</td>
                            <td>@{{ job.hours_worked }}</td>
                            <td class="mdl-data-table__cell--non-numeric">@{{ job.details }}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <mdl-checkbox v-model="job.completed" class="is-disabled " disabled></mdl-checkbox>
                            </td>
                            <td>@{{ job.price }}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <mdl-checkbox v-model="job.paid" class="is-disabled" disabled></mdl-checkbox>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <mdl-button icon="edit" @click.native="showEditJobPage(key)"></mdl-button>
                            </td>
                        </tr>
                        <tr v-if="search && !jobs.length">
                            <td colspan="12" class="mdl-data-table__cell--non-numeric">
                                No results found for "@{{ search }}"
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="mdl-data-table__cell--non-numeric">
                                <div class="mdl-grid">
                                    <div class="mdl-layout-spacer"></div>
                                    <div class="mdl-cell mdl-cell--1-col">
                                        <mdl-spinner v-if="jobsLoading"></mdl-spinner>
                                        <mdl-button v-else
                                                    colored
                                                    icon="add"
                                                    @click.native="showNewJobPage"></mdl-button>
                                    </div>
                                    <div class="mdl-layout-spacer"></div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!--</editor-fold>-->
            </div>
        </div>
    </main>
    <footer class="mdl-mini-footer">
        <div class="mdl-mini-footer__right-section">
            <?= round((microtime(true) - LUMEN_START) * 1000, 2) . 'ms' ?>
        </div>
    </footer>

    <!--<editor-fold desc="Edit Job Dialog">-->
    <mdl-dialog ref="newJobPage" title="Add a job">
        <form ref="newJobForm" @submit.prevent="saveJob">
            {{-- Name Section --}}
            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--3-col mdl-typography--text-center">
                    Name
                </div>
                <div class="mdl-cell mdl-cell--9-col">
                    <mdl-textfield floating-label="First Name"
                                   class="mdl-textfield--full-width"
                                   :class="{ 'is-dirty': editedJob.name }"
                                   v-model="editedJob.name"
                                   required></mdl-textfield>
                    <mdl-textfield floating-label="Surname"
                                   class="mdl-textfield--full-width"
                                   :class="{ 'is-dirty': editedJob.surname }"
                                   v-model="editedJob.surname"
                                   required></mdl-textfield>
                </div>
            </div>
            <hr>
            {{-- Address Section --}}
            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--3-col mdl-typography--text-center">
                    Address
                </div>
                <div class="mdl-cell mdl-cell--9-col mdl-grid mdl-grid--no-spacing">
                    <div class="mdl-cell mdl-cell--12-col">
                        <mdl-textfield floating-label="Address"
                                       class="mdl-textfield--full-width"
                                       :class="{ 'is-dirty': editedJob.address }"
                                       v-model="editedJob.address"
                                       required></mdl-textfield>
                    </div>
                    <div class="mdl-cell mdl-cell--8-col">
                        <mdl-textfield floating-label="Suburb"
                                       class="mdl-textfield--full-width"
                                       :class="{ 'is-dirty': editedJob.suburb }"
                                       v-model="editedJob.suburb"
                                       required></mdl-textfield>
                    </div>
                    <div class="mdl-layout-spacer"></div>
                    <div class="mdl-cell mdl-cell--3-col">
                        <mdl-textfield floating-label="Postcode"
                                       pattern="3[0-9]{3}"
                                       class="mdl-textfield--full-width"
                                       :class="{ 'is-dirty': editedJob.postcode }"
                                       v-model="editedJob.postcode"
                                       required></mdl-textfield>
                    </div>
                </div>
            </div>
            <hr>
            {{-- Details Section --}}
            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--3-col mdl-typography--text-center">
                    Details
                </div>
                <div class="mdl-cell mdl-cell--9-col mdl-grid mdl-grid--no-spacing">
                    <div class="mdl-cell mdl-cell--3-col">
                        <mdl-textfield floating-label="Date"
                                       type="date"
                                       class="has-placeholder mdl-textfield--full-width"
                                       :class="{ 'is-dirty': editedJob.date }"
                                       v-model="editedJob.date"
                                       required></mdl-textfield>
                    </div>
                    <div class="mdl-layout-spacer"></div>
                    <div class="mdl-cell mdl-cell--3-col">
                        <mdl-textfield floating-label="Hours Worked"
                                       type="number"
                                       class="mdl-textfield--full-width"
                                       :class="{ 'is-dirty': editedJob.hours_worked }"
                                       v-model="editedJob.hours_worked"
                                       required></mdl-textfield>
                    </div>
                    <div class="mdl-layout-spacer"></div>
                    <div class="mdl-cell mdl-cell--4-col mdl-cell--middle">
                        <mdl-checkbox v-model="editedJob.completed" class="mdl-js-ripple-effect">Completed
                        </mdl-checkbox>
                    </div>
                    <div class="mdl-cell mdl-cell--7-col">
                        <mdl-textfield floating-label="Price"
                                       pattern="\$[0-9]+(\.[0-9]{2})?"
                                       class="mdl-textfield--full-width"
                                       :class="{ 'is-dirty': editedJob.price }"
                                       v-model="editedJob.price"
                                       required></mdl-textfield>
                    </div>
                    <div class="mdl-layout-spacer"></div>
                    <div class="mdl-cell mdl-cell--4-col mdl-cell--middle">
                        <mdl-checkbox v-model="editedJob.paid" class="mdl-js-ripple-effect">Paid</mdl-checkbox>
                    </div>
                    <div class="mdl-cell mdl-cell--12-col">
                        <mdl-textfield floating-label="Details"
                                       textarea
                                       rows="4"
                                       class="mdl-textfield--full-width"
                                       :class="{ 'is-dirty': editedJob.details }"
                                       v-model="editedJob.details"></mdl-textfield>
                    </div>
                </div>
            </div>

            <div class="mdl-grid">
                <mdl-tooltip target="delete-button" large>Delete Job</mdl-tooltip>
                <mdl-button accent
                            icon="delete forever"
                            id="delete-button"
                            v-show="!jobIsNew"
                            type="button"
                            @click.native="deleteJob">
                    <i class="material-icons">delete forever</i>
                </mdl-button>
                <div class="mdl-layout-spacer"></div>
                <mdl-button @click.native="$refs.newJobPage.close" type="button">Cancel</mdl-button>
                <mdl-button primary type="submit">Save</mdl-button>
            </div>
            <mdl-snackbar display-on="saveError"></mdl-snackbar>

        </form>

        <template slot="actions"></template>
    </mdl-dialog>
    <!--</editor-fold>-->

    <mdl-snackbar display-on="fetchError"></mdl-snackbar>
    <mdl-snackbar display-on="saveSuccess"></mdl-snackbar>
    <mdl-snackbar display-on="deleteSuccess"></mdl-snackbar>

</div>

<script>
    Vue.use(VueMdl.default);
    const vm = new Vue({
        el: '#app',
        data: {
            jobs: {!! $jobs->toJson() !!}, // Include the jobs list with the initial page load
            editedJob: { // Model for the edit job dialog
                id: null,
                name: '',
                surname: '',
                address: '',
                suburb: '',
                postcode: '',
                date: '',
                hours_worked: null,
                details: null,
                completed: false,
                price: '',
                paid: false,
            },
            _editedJob: {},
            jobIsNew: true,
            jobsLoading: false,
            search: '',
            searchTimeout: null,
        },
        created() {
            // Copy the empty job form so we can reset it later
            this._editedJob = Object.assign({}, this.editedJob)
        },
        methods: {
            showNewJobPage() {
                this.editedJob = Object.assign({}, this._editedJob);
                this.jobIsNew  = true;
                this.$refs.newJobPage.open();
            },
            showEditJobPage(jobKey) {
                this.editedJob = Object.assign({}, this.jobs[jobKey]);
                this.jobIsNew  = false;
                this.$refs.newJobPage.open();
            },
            saveJob() {
                if (this.jobIsNew) {
                    axios.post('/api/jobs', this.editedJob).then(
                        res => saveSuccess.bind(this)(),
                        err => saveError.bind(this, err)()
                    )
                } else {
                    axios.patch('/api/jobs/' + this.editedJob.id, this.editedJob).then(
                        res => saveSuccess.bind(this)(),
                        err => saveError.bind(this, err)()
                    )
                }

                function saveSuccess() {
                    this.$refs.newJobPage.close();
                    this.$root.$emit('saveSuccess', {message: 'Job Saved',});
                    this.fetchJobs();
                }

                function saveError(err) {
                    this.$root.$emit('saveError', {
                        message: 'Failed to save job: ' + err.message,
                        timeout: 3500,
                    });
                }
            },
            deleteJob() {
                axios.delete('/api/jobs/' + this.editedJob.id).then(res => {
                    this.$refs.newJobPage.close();
                    this.$root.$emit('deleteSuccess', {
                        message: 'Job deleted',
                    });
                    this.fetchJobs();
                }, err => {
                    this.$root.$emit('saveError', {
                        message: 'Failed to delete job: ' + err.message,
                        timeout: 3500,
                    });
                })
            },
            fetchJobs() {
                this.jobsLoading = true;
                axios.get('/api/jobs').then(res => {
                    this.jobs        = res.data;
                    this.jobsLoading = false;
                }, err => {
                    this.jobsLoading = false;
                    this.$root.$emit('fetchError', {
                        message: 'Failed to fetch jobs: ' + err.message,
                        timeout: 3500,
                        actionHandler: this.fetchJobs,
                        actionText: 'Retry'
                    })
                })
            }
        },
        watch: {
            search(newValue) {
                this.jobsLoading = true;
                window.clearTimeout(this.searchTimeout);
                if (newValue === '') {
                    this.fetchJobs();
                    return;
                }
                this.searchTimeout = window.setTimeout(() => {
                    axios.get('/api/jobs/search/' + newValue).then(res => {
                        this.jobs        = res.data;
                        this.jobsLoading = false;
                    }, err => {
                        this.jobsLoading = false;
                        this.$root.$emit('fetchError', {
                            message: 'Failed to search jobs: ' + err.message,
                            timeout: 3500,
                            actionHandler: this.fetchJobs,
                            actionText: 'Retry'
                        })
                    });
                }, 750);
            }
        },
    });
</script>

</body>
</html>
