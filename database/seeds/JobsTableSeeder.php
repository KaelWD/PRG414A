<?php
declare(strict_types = 1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param \Faker\Generator $faker
     *
     * @return void
     */
    public function run (Faker\Generator $faker)
    {
        $recordCount = 10;

        foreach (range(1, $recordCount) as $index) {
            DB::table('jobs')->insert(
                [
                    'name' => $faker->firstName(),
                    'surname' => $faker->lastName(),
                    'address' => $faker->streetAddress(),
                    'suburb' => $faker->city(),
                    'postcode' => $faker->numberBetween(3000, 3999),
                    'date' => $faker->date('Y-m-d'),
                    'hours_worked' => random_int(0, 50),
                    'completed' => random_int(0, 1),
                    'price' => random_int(999, 99999),
                    'paid' => random_int(0, 1)
                ]
            );
        }
    }
}
