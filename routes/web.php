<?php
declare(strict_types = 1);

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use Illuminate\Http\Request;

// Display the jobs page
$app->get('/', function () use ($app) {
    $jobs = App\Job::query()->get();

    return view('app', compact('jobs'));
});

// Refresh the job list
$app->get('/api/jobs', function (Request $request) use ($app) {
    return App\Job::query()->get()->toJson();
});

// Add a new job
$app->post('/api/jobs', function (Request $request) use ($app) {
    /** @var \App\Job $job */
    $job = new App\Job();
    $job->fill($request->all());

    return response()->json($job->saveOrFail());
});

// Edit an existing job
$app->patch('/api/jobs/{job}', function ($job, Request $request) use ($app) {
    /** @var \App\Job $job */
    $job = App\Job::query()->findOrFail($job);
    $job->fill($request->all());

    return response()->json($job->saveOrFail());
});

// Delete a job
$app->delete('/api/jobs/{job}', function ($job) use ($app) {
    /** @var \App\Job $job */
    $job = App\Job::query()->findOrFail($job);

    return response()->json($job->delete());
});

// Search for a job
$app->get('/api/jobs/search/{search}', function (string $search) use ($app) {
    // Decode the url so we can use spaces and special characters
    $search = rawurldecode($search);

    $results = App\Job::query()->where('name', 'LIKE', "%$search%")
                      ->orWhere('surname', 'LIKE', "%$search%")
                      ->orWhere('address', 'LIKE', "%$search%")
                      ->orWhere('suburb', 'LIKE', "%$search%")
                      ->orWhere('postcode', 'LIKE', "%$search%")
                      ->get();

    return $results->toJson();
});
