<?php
declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\ValidationException;

class Job extends Model
{
    public $timestamps = false;

    protected $casts = [
        'postcode'  => 'string',
        'completed' => 'boolean',
        'paid'      => 'boolean',
    ];

    protected $dates = [
        'date',
    ];

    protected $dateFormat = "Y-m-d";

    protected $fillable = [
        'name',
        'surname',
        'address',
        'suburb',
        'postcode',
        'date',
        'hours_worked',
        'details',
        'completed',
        'price',
        'paid',
    ];

    public function setPostcodeAttribute (string $value)
    {
        if ($value < 3000 || $value >= 4000) throw new ValidationException('Postcode must be between 3000 and 4000');
        $this->attributes['postcode'] = (string)$value;
    }

    public function setPriceAttribute (string $value)
    {
        $splitPrice = explode('.', $value);
        if (count($splitPrice) > 2 || count($splitPrice) < 1) throw new ValidationException('Invalid currency format');

        $dollars = (int)str_replace('$', '', $splitPrice[0]);
        $cents = (int)$splitPrice[1] ?? 0;

        $this->attributes['price'] = $dollars * 100 + $cents;
    }

    public function getPriceAttribute (int $value)
    {
        $value = (string)$value;
        $length = strlen($value);

        $dollars = $length > 2 ? substr($value, 0, -2) : '0';
        $cents = $length > 1 ? substr($value, -2) : '0' . $value;

        return "\$$dollars.$cents";
    }
}
