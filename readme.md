# Lumen PHP Framework

## Installing

 - Unpack ZIP
 - Install PHP 7, Composer, and MariaDB
 - Run `composer create-project --prefer-source`
 - Create a new database and set the credentials in `.env`
 - Use PHP's built-in web server with `php -S localhost:8000 public/index.php`
